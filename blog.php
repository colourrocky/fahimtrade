<?php 


/*

Template Name: blog


*/





get_header(); ?> 
<!-- header_aria-->
        <div class="longerDays_aria">
             <div class="longerDays_aria_overlay"></div>
              <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                       <div class="longerDays_text">
                        <h2>Take a tour of longerDays</h2>
                        <p>Meet the last virtual assistance company you will ever hire</p>
                        <div class="longerDays_cercel ">
                           <img class="longerDays_cercel_img1" src=" <?php echo get_template_directory_uri(); ?>/img/shape_circle_white.png" alt="">
                            <img class="longerDays_cercel_img2" src="<?php echo get_template_directory_uri(); ?>/img/ProServices.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="service_was_designed_aria">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="service_was_designed">
                            <h2>Our service was designed with small businesses in mind.</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="main_content_Feature_aria">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="main_content_Feature mahdi">
                           <img class="img-circle" src="<?php echo get_template_directory_uri(); ?>/img/playmaking.png" alt="">
                           
                            <h2>We Have Your Match</h2>
                                
                            <p>Our Team Leads are like super employees, powered by the                                         combined skills and talents of many Virtual Assistants. Our                                     entire team works from the same office, collaborating and                                     communicating with your Team Lead, who will coordinate                                      your tasks across all of our services. You gain access                                     to our entire skill set, but only need to remember one name</p>
                            <a href=""class="">Red more...</a>
                        </div>
                        <div class="main_content_Feature mahdi">
                           <img class="img-circle" src="<?php echo get_template_directory_uri(); ?>/img/ProServices.jpg" alt="">
                            <h2>We Have Your Match</h2>
                                
                            <p>Our Team Leads are like super employees, powered by the                                         combined skills and talents of many Virtual Assistants. Our                                     entire team works from the same office, collaborating and                                     communicating with your Team Lead, who will coordinate                                      your tasks across all of our services. You gain access                                     to our entire skill set, but only need to remember one name</p>
                            <a href=""class="">Red more...</a>
                        </div>
                        <div class="main_content_Feature mahdi">
                           <img class="img-circle" src="<?php echo get_template_directory_uri(); ?>/img/playmaking.png" alt="">
                            <h2>We Have Your Match</h2>
                                
                            <p>Our Team Leads are like super employees, powered by the                                         combined skills and talents of many Virtual Assistants. Our                                     entire team works from the same office, collaborating and                                     communicating with your Team Lead, who will coordinate                                      your tasks across all of our services. You gain access                                     to our entire skill set, but only need to remember one name</p>
                            <a href=""class="">Red more...</a>
                        </div>
                        <div class="main_content_Feature mahdi">
                           <img class="img-circle" src="<?php echo get_template_directory_uri(); ?>/img/playmaking.png" alt="">
                            <h2>We Have Your Match</h2>
                                
                            <p>Our Team Leads are like super employees, powered by the                                         combined skills and talents of many Virtual Assistants. Our                                     entire team works from the same office, collaborating and                                     communicating with your Team Lead, who will coordinate                                      your tasks across all of our services. You gain access                                     to our entire skill set, but only need to remember one name</p>
                            <a href=""class="">Red more...</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="getstarted_aria">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="getstarted">
                            <h2>Do what you do best – we’ll handle the rest!</h2>
                            <p>LongerDays Virtual Assistants can help you reduce tedium and gain access to a skill set you just won’t find in a single office employee.</p>
                            <a href=""><i class="fa fa-check"></i>Get Started </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!--footer_aria-->

<?php get_footer(); ?> 