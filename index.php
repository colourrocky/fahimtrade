<?php get_header(); ?> 

        <div class="Contact_to_aria">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                     

                
                
                
                 
                       <div class="Contact_to">
                         <h3>One Contact to Get All of Your Work Done</h3>
                          <p>Your Team Lead can coordinate your tasks across all of our services:</p>
                           
                              <?php

                             // The Query
                             query_posts( array ( 'category_name' => 'product', 'posts_per_page' => 8 ) );

                             // The Loop
                            while ( have_posts() ) : the_post(); ?>
                            <div class="col-md-3">
                                <div class="contact_to_singel wow bounceIn animated">
                               <?php the_post_thumbnail('product_image', array('class' => 'img-thumbnail img-responsive')); ?>
                               <a href="<?php the_permalink()?>"><h2><?php echo wp_trim_words( get_the_title(), 3, '...' );?></h2></a>
                               </div>
                            </div>

                             <?php endwhile;

                             // Reset Query
                             wp_reset_query();                           ?>

                        
                         
                       </div>
                    </div>
                        <a href="http://localhost/newsnuri/category/product/" class="Contact_to_button">Learn More About Our product </a>
                </div>
            </div>
        </div>
        <div class="LongerDays_aria">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="LongerDays">
                        <h2>LongerDays is Designed to Make It Easy for You</h2>
                        <p>Feast your eyes on our fantastic features:</p>
                           
                               <?php

                             // The Query
                             query_posts( array ( 'category_name' => 'small_product', 'posts_per_page' => 7 ) );

                             // The Loop
                            while ( have_posts() ) : the_post(); ?>
                            <div class="singel_LongerDays">
                                 <?php the_post_thumbnail('product_image', array('class' => 'img-thumbnail img-responsive')); ?>
                                 <a href="<?php the_permalink()?>"><h3><?php echo wp_trim_words( get_the_title(), 3, '...' );
?></h3></a>
                            </div>

                             <?php endwhile;

                             // Reset Query
                             wp_reset_query();

                             ?>


                           
                            
                          
                        </div>
                    </div>
                          <a href="" class="Features">Take a Tour of Our Features</a>
                </div>
            </div>
        </div>
        <div class="virtual_assistance_aria">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                       <div class="virtual_assistance">
                           <p>Not quite sure what virtual assistance is, or how it can work for you</p>
                       </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="subheader">
                        <a href="">Click Here To Find Out More.</a>
                    </div>
                    </div>
          </div>
            </div>
        </div>
        <div class="Businesses_Depend_aria">
            <div class="container">
                <div class="row">
                    
                        <div class="col-lg-12">
                           <div class="Businesses_Depend">
                            <h1>Businesses Depend On Us</h1>
                            <p>What our customers are saying about LongerDays:</p>
                            

                           <div class="row">
                              <?php

                             // The Query
                             query_posts( array ( 'category_name' => 'newarave', 'posts_per_page' => 3 ) );

                             // The Loop
                            while ( have_posts() ) : the_post(); ?>
                            <div class="col-lg-4">
                                <div class="item-hover circle effect20 bottom_to_top button_margin"><a href="<?php the_permalink();?>">
                                  <div class="img"> <?php the_post_thumbnail()?></div>
                                  <div class="info">
                                    <div class="info-back">
                                      <h3><?php echo wp_trim_words( get_the_title(), 3, '...' );?></h3>
                                      <p><?php the_excerpt();?></p>
                                    </div>
                                  </div></a>
                                  </div>
                            </div>

                             <?php endwhile;

                             // Reset Query
                             wp_reset_query();

                             ?>


                        
                           </div>


                            <a href="" class=" Success_Stories">View More Success Stories</a> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<?php get_footer(); ?> 