<?php get_header(); ?> 


			
	
			 
		
	



        		<?php if(have_posts()) : ?>
		<?php while(have_posts())  : the_post(); ?>
        <div class="longerDays_aria">
             <div class="longerDays_aria_overlay"></div>
              <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                       <div class="longerDays_text">
                        <h2> <?php
echo wp_trim_words( get_the_title(), 2, '...' );
?></h2>
                                                   
                        <p>Meet the last virtual assistance company you will ever hire</p>
                        <div class="longerDays_cercel ">
                           <img alt="" src="<?php echo get_template_directory_uri(); ?>/img/shape_circle_white.png" class="longerDays_cercel_img1">
                            
                            <?php the_post_thumbnail(array('class' => ' longerDays_cercel_img2')); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                	<?php endwhile; ?>
		<?php else : ?>
			<h3><?php _e('404 Error&#58; Not Found'); ?></h3>
		<?php endif; ?>
<div class="single_page_text_area">
           
<div class="container">
            
<div class="row">
    <div class="col-md-8">
        		<?php if(have_posts()) : ?>
		<?php while(have_posts())  : the_post(); ?>
       
        <div class="single_page_blog_area">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                       
                        <div class="single_page_blog_img">
                            <?php the_post_thumbnail(); ?>
                            <div class="post_info">
                                Posted In: <?php the_category(', '); ?> | Posted on: <?php the_time('M d, Y') ?> <?php comments_popup_link('No Comment', '1 Comment', '% Comments'); ?>
                            </div>
                            <h2><?php the_title();?></h2>
                            <?php the_content();?>
                            
                            

                           
                           
                            <?php comments_template( '', true ); ?> 
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        
        	<?php endwhile; ?>
		<?php else : ?>
			<h3><?php _e('404 Error&#58; Not Found'); ?></h3>
		<?php endif; ?>
    </div>
    <div class="col-md-4">
        <?php dynamic_sidebar('side_menu'); ?>
    </div>
</div>
</div>
</div>
<?php get_footer(); ?> 
