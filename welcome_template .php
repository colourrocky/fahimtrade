<?php

/*

Template Name: welcome template 


*/

get_header(); ?> 
<?php get_template_part('slider'); ?>
        <div class="Contact_to_aria">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                       <div class="Contact_to">
                         <h3>One Contact to Get All of Your Work Done</h3>
                          <p>Your Team Lead can coordinate your tasks across all of our services:</p>
                                  <div class="contact_to_singel wow bounceInRight animated">
                               <img src="<?php echo get_template_directory_uri(); ?>/img/graph.png" alt="" class="img-circle"class="img-responsive"class="img-responsive"class="img-responsive"class="img-responsive">
                               <h2>Administrative Assistance</h2>
                               </div>
                                  <div class="contact_to_singel wow bounceInRight animated">
                               <img src="<?php echo get_template_directory_uri(); ?>/img/graph.png" alt="" class="img-circle"class="<?php echo get_template_directory_uri(); ?>/img-responsive"class="img-responsive"class="img-responsive">
                               <h2>Administrative Assistance</h2>
                               </div>
                                  <div class="contact_to_singel wow bounceInRight animated">
                               <img src="<?php echo get_template_directory_uri(); ?>/img/graph.png" alt="" class="img-circle"class="img-responsive"class="img-responsive">
                               <h2>Administrative Assistance</h2>
                               </div>
                                  <div class="contact_to_singel  wow bounceInRight animated">
                               <img src="<?php echo get_template_directory_uri(); ?>/img/graph.png" alt="" class="img-circle"class="img-responsive">
                               <h2>Administrative Assistance</h2>
                               </div>
                                  <div class="contact_to_singel wow bounceInRight animated">
                               <img src="<?php echo get_template_directory_uri(); ?>/img/graph.png" alt="" class="img-circle"class="img-responsive">
                               <h2>Administrative Assistance</h2>
                               </div>
                            
                        <a href="" class="Contact_to_button">Learn More About Our Services </a>
                       </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="LongerDays_aria">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="LongerDays">
                        <h2>LongerDays is Designed to Make It Easy for You</h2>
                        <p>Feast your eyes on our fantastic features:</p>
                            <div class="singel_LongerDays">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/playmaking_plain.png" alt=""class="img-responsive"class="img-responsive"class="img-responsive">
                                <h3>A Dedicated Point of Contact</h3>
                            </div>
                            <div class="singel_LongerDays">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/playmaking_plain.png" alt=""class="img-responsive"class="img-responsive"class="img-responsive"class="img-responsive">
                                <h3>A Dedicated Point of Contact</h3>
                            </div>
                            <div class="singel_LongerDays">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/playmaking_plain.png" alt=""class="img-responsive"class="img-responsive"class="img-responsive"class="img-responsive"class="img-responsive">
                                <h3>A Dedicated Point of Contact</h3>
                            </div>
                            <div class="singel_LongerDays">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/playmaking_plain.png" alt=""class="img-responsive"class="img-responsive">
                                <h3>A Dedicated Point of Contact</h3>
                            </div>
                            <div class="singel_LongerDays">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/playmaking_plain.png" alt=""class="img-responsive">
                                <h3>A Dedicated Point of Contact</h3>
                            </div>
                            <div class="singel_LongerDays">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/playmaking_plain.png" alt=""class="img-responsive"class="img-responsive">
                                <h3>A Dedicated Point of Contact</h3>
                            </div>
                            <div class="singel_LongerDays">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/playmaking_plain.png" alt=""class="img-responsive">
                                <h3>A Dedicated Point of Contact</h3>
                            </div>
                        </div>
                          <a href="" class="Features">Take a Tour of Our Features</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="virtual_assistance_aria">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                       <div class="virtual_assistance">
                           <p>Not quite sure what virtual assistance is, or how it can work for you</p>
                       </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="subheader">
                        <a href="">Click Here To Find Out More.</a>
                    </div>
                    </div>
          </div>
            </div>
        </div>
        <div class="Businesses_Depend_aria">
            <div class="container">
                <div class="row">
                    
                        <div class="col-lg-12">
                           <div class="Businesses_Depend">
                            <h1>Businesses Depend On Us</h1>
                            <p>What our customers are saying about LongerDays:</p>
                            

                           <div class="row">
                                <div class="col-lg-4">
                                <div class="item-hover circle effect20 bottom_to_top button_margin"><a href="#">
                                  <div class="img"><img alt="img" src="<?php echo get_template_directory_uri(); ?>/img/circle-american.png"></div>
                                  <div class="info">
                                    <div class="info-back">
                                      <h3>Heading here</h3>
                                      <p>Description goes here</p>
                                    </div>
                                  </div></a>
                                  </div>
                            </div>
                                <div class="col-lg-4">
                                <div class="item-hover circle effect20 bottom_to_top button_margin"><a href="#">
                                  <div class="img"><img alt="img" src="<?php echo get_template_directory_uri(); ?>/img/circle-american.png"></div>
                                  <div class="info">
                                    <div class="info-back">
                                      <h3>Heading here</h3>
                                      <p>Description goes here</p>
                                    </div>
                                  </div></a>
                                  </div>
                            </div>
                                <div class="col-lg-4">
                                <div class="item-hover circle effect20 bottom_to_top button_margin"><a href="#">
                                  <div class="img"><img alt="img" src="<?php echo get_template_directory_uri(); ?>/img/circle-american.png"></div>
                                  <div class="info">
                                    <div class="info-back">
                                      <h3>Heading here</h3>
                                      <p>Description goes here</p>
                                    </div>
                                  </div></a>
                                  </div>
                            </div>
                           </div>


                            <a href="" class=" Success_Stories">View More Success Stories</a> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<?php get_template_part('Talk'); ?>
		<?php get_footer(); ?> 