<?php
include_once('inc/theme_feils.php');
add_theme_support( 'post-thumbnails' );

		add_image_size('product_image',280, 280, true);
    	add_theme_support( 'title-tag' );
    add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );

add_theme_support( 'custom-logo', array(
	'height'      => 50,
	'width'       => 50,
	'flex-height' => true,
	'flex-width'  => true,
	'header-text' => array( 'site-title', 'site-description' ),
) );
add_theme_support( 'custom-background', apply_filters( 'twentyfifteen_custom_background_args', array(
		'default-color'      => $default_color,
		'default-attachment' => 'fixed',
	) ) );




		function fahem_tred_register_menu() {
		
				register_nav_menu( 'primary', __( 'Primary Menu', 'theme-slug' ) );
			}
	

		add_action('after_setup_theme', 'fahem_tred_register_menu');
	



function comment_scripts(){

   if ( is_singular() ) wp_enqueue_script( 'comment-reply' );

}

add_action( 'wp_enqueue_scripts', 'comment_scripts' );







function footer_widget_areas() {
	register_sidebar( array(
		'name' => __( 'Footer menu', 'mahdi' ),
		'id' => 'footer_menu',
		'before_widget' => '<div class="col-lg-3">
                        <div class="Services">',
		'after_widget' => '</div></div>',
	    'before_title' => '<h2>',
	    'after_title' => '</h2>',
	) );
}
add_action('widgets_init', 'footer_widget_areas');




function side_widget_areas() {
	register_sidebar( array(
		'name' => __( 'side menu', 'mahdi' ),
		'id' => 'side_menu',
		'before_widget' => '<div class="Services">',
		'after_widget' => '</div>',
	    'before_title' => '<h2>',
	    'after_title' => '</h2>',
	) );
}
add_action('widgets_init', 'side_widget_areas');



function wpexplorer_login_logo() { ?>
	<style type="text/css">
		body.login div#login h1 a {
			background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/cropped-2.png );
			padding-bottom: 30px;
		}
        .login.login-action-login.wp-core-ui.locale-en-us > h1 {  font-family: arial;margin-top: 100px}
      
	</style>
	<h1>Fahim trade intermational</h1>
	
<?php }
add_action( 'login_enqueue_scripts', 'wpexplorer_login_logo' );


?>