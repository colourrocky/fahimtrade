<?php 

/*

Template Name: single_sarvese


*/

get_header(); ?> 
<!-- header_aria-->
        <div class="longerDays_aria">
             <div class="longerDays_aria_overlay"></div>
              <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                       <div class="longerDays_text">
                        <h2>Our Services</h2>
                        <p>Five services, one simple price.</p>
                        <div class="longerDays_cercel ">
                           <img class="longerDays_cercel_img1" src=" <?php echo get_template_directory_uri(); ?>/img/shape_circle_white.png" alt="">
                            <img class="img-circle longerDays_cercel_img2" src="<?php echo get_template_directory_uri(); ?>/img/servece.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="service_was_designed_aria">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="service_was_designed">
                            <h2>Run your business without wearing so many hats. </h2>
                            <p>LongerDays combines the skills and talents of many virtual assistants into a single, powerful team.
Our flat rate subscription plans includes access to all of our services..</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--content-->
        <div class="Assistance_aria">
            <div class="container">
                <div class="col-lg-12">
                   
                  
                   <?php

                             // The Query
                             query_posts( array ( 'category_name' => 'product', 'posts_per_page' => -1) );

                             // The Loop
                            while ( have_posts() ) : the_post(); ?>
                             <div class="Assistance">
                            
                        <?php the_post_thumbnail('product_image', array('class' => 'img-thumbnail img-responsive')); ?>
                        <h2><?php the_title();?></h2>
                        <?php the_excerpt(); ?>
                  
                    </div>

                             <?php endwhile;

                             // Reset Query
                             wp_reset_query();

                             ?>

                </div>
            </div>
        </div>
        <!--content-->
<?php get_template_part('Talk'); ?>
<!--footer_aria-->
<?php get_footer(); ?> 