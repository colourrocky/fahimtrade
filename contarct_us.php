<?php 

/*

Template Name: contact us


*/

get_header(); ?> 
        <div class="longerDays_aria">
             <div class="longerDays_aria_overlay"></div>
              <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                       <div class="longerDays_text">
                        <h2>Contart us</h2>
                        <p>Five services, one simple price.</p>
                        <div class="longerDays_cercel ">
                           <img class="longerDays_cercel_img1" src="<?php echo get_template_directory_uri(); ?>/img/shape_circle_white.png" alt="">
                            <img class="img-circle longerDays_cercel_img2" src="<?php echo get_template_directory_uri(); ?>/img/cup.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="service_was_designed_aria">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="service_was_designed">
                            <h2>Browse our helpful documentation:</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       <!--content-->
           <div class="cotract_us_aria">
               <div class="container">
                   <div class="row">
                   <div class="col-lg-3"></div>
                   <div class="col-lg-6">
                       
                       <div class="cotract_us">
                            <?php echo do_shortcode( '[contact-form-7 id="108" title="contract from 2"]' ); ?>
                         
                       </div>
                   </div>
                   <div class="col-lg-3"></div>
                   </div>
               </div>
           </div>
           <div class="cotract_us_text_aria">
               <div class="container">
                   <div class="row">
                       <div class="col-lg-12">
                           <div class="cotract_us_text">
                               <div class="col-lg-4">
                                   <div class="cotract_us_text_one">
                                   <h2>Already a customer?</h2>
                                   <p>If you are already a customer, feel free to contact your Team Lead via phone or email to ask any questions regarding our services. They'll either have the answer right away, or can get the answer to you quickly!</p>
                                   </div>
                                   </div>
                               <div class="col-lg-4">
    <div class="cotract_us_text_center"><h2>Or</h2>
                              </div>
                               </div>
                               <div class="col-lg-4">
                                   <div class="cotract_us_text_tow">
                                       <h2>Not a customer yet?</h2>
                                       <p>You might not be a customer yet, but you'd probably like to talk to a human, wouldn't you? We've got representatives available to discuss how we can help improve your business by taking over the time consuming tasks you'd rather not be focusing your time on.</p>
                                       <div class="Request_to_talk_contract_us">
                        <a href="">Request to talk</a>
                        
                    </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       
       <!--content-->
<?php get_template_part('Talk'); ?>
<?php get_footer(); ?> 